# Rede Neural Artificial para reconhecimento de funcao de
# Transferencia variante no tempo

import numpy as np 
from scipy import signal as sgn
import matplotlib.pyplot as plt


num1 = [1]
den1 = [1, 0.2, 1]
Csys = sgn.TransferFunction(num1,den1) #FT continua ate t=79.9

num2 = [3]
den2 = [1, 2, 1]
Csys2 = sgn.TransferFunction(num2, den2) #FT continua de t = 80 ate t = 120

(t1, T_amostr) = np.linspace(0,79.9,num=800,endpoint=True, retstep= True) # num = (t_final + 0.1)/ Tab
(t2, T_amostr2) = np.linspace(80,119.9,num=400,endpoint=True, retstep= True) # num = (t_final - t_final + 0.1)/ Tab

Dsys = sgn.cont2discrete((num1, den1), 0.1, method='zoh') # discretizacao do sistema com scipy
Dsys2 = sgn.cont2discrete((num2, den2), 0.1, method='zoh')


data=np.genfromtxt('idinp.csv',dtype=float,delimiter=',',skip_header=1) #leitura dos dados de input
inrand1 = data[:,0] #coluna 0: banda [0 0.01]

in1 = inrand1[0:800]
out80 = sgn.convolve(Dsys, inrand1) #convolucao de G1(z) com o vetor de entrada pseudo randômico

print(Dsys)